+++
# date = 2020-02-03T14:22:07-06:00 # date from hugo new test-page.md
# date = 2020-04-18T23:00:00Z # date from Elmhurst Soiree content
date = 2020-02-03T06:00:00Z # date from Forestry, fresh


additional_info = "See below for ticket information"
category = "Fundraiser"
cta_label = ""
cta_url = ""
end_date = 2020-04-19T05:00:00Z
excerpt = "Attend our annual fundraiser to support the museum's mission."
hero_caption = ""
hero_image = "/v1579666352/Events/Save_the_Date_gqohv2.jpg"
related_exhibition = ["exhibitions/par-excellence-redux.md"]
start_date = 2020-04-18T23:00:00Z
title = "Soiree 2020"


+++
Presented by the Museum’s Sustaining Fellows, Soirée 2020 will include cocktails, a multi-course dinner, entertainment, and live and silent auctions filled with artworks by established and emerging artists as well as a fabulous selection of vacations and experiences.

The event will also be a sneak-peek of the museum's exhibition Par Excellence Redux - a fully playable 18-hole mini golf course designed by artists.

We invite you to join our efforts to inspire people of all ages. Proceeds of Soirée 2020 will be used to help the Museum enrich people’s lives by deepening their knowledge of art, architecture and design, increasing their understanding of the relevance of visual art in our society and sparking the development of individual creativity

[Collector Table ($5,000)](https://eam-shop.myshopify.com/products/soiree-2020-april-18-2020-collector-table?_pos=2&_sid=60d48ad5a&_ss=r)

[Patron Table ($3,500)](https://eam-shop.myshopify.com/products/soiree-2020-april-18-2020-patron-table?_pos=12&_sid=2c8c9afb1&_ss=r&utm_source=2020+Soir%C3%A9e+Prospective+Host+Committee&utm_campaign=f253715505-EMAIL_CAMPAIGN_2017_10_18_COPY_02&utm_medium=email&utm_term=0_614e631dd3-f253715505-274466525&mc_cid=f253715505&mc_eid=9bd989e7a3) - price available until March 15

[Tickets ($375)](https://eam-shop.myshopify.com/products/copy-of-soiree-2019-april-27-patron-table?_pos=14&_sid=2c8c9afb1&_ss=r&utm_source=2020+Soir%C3%A9e+Prospective+Host+Committee&utm_campaign=f253715505-EMAIL_CAMPAIGN_2017_10_18_COPY_02&utm_medium=email&utm_term=0_614e631dd3-f253715505-274466525&mc_cid=f253715505&mc_eid=9bd989e7a3)

Buy a [Golden Ticket ($100)](https://eam-shop.myshopify.com/products/soiree-2019-golden-ticket) for a chance to win $3,000 cash.

I cannot attend, but would like to make a [Donation](https://eam-shop.myshopify.com/products/donation).
